package n.qsoft.rxjavafirst;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class RxJavaActivity extends AppCompatActivity {
    private static final String TAG = RxJavaActivity.class.getSimpleName();
    private CompositeDisposable disposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rx_java);

        disposable.add(getDescriptionsObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<Description, Description>() {
                    @Override
                    public Description apply(Description description) throws Exception {
                        // Making the description to all uppercase
                        description.setDescription(description.getDescription().toUpperCase());
                        return description;
                    }
                })
                .subscribeWith(getDescriptionsObserver()));
    }

    private DisposableObserver<Description> getDescriptionsObserver() {
        return new DisposableObserver<Description>() {

            @Override
            public void onNext(Description description) {
                Log.d(TAG, "Note: " + description.getDescription());
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "All descriptions are emitted!");
            }
        };
    }

    private Observable<Description> getDescriptionsObservable() {
        final List<Description> descriptionList = prepareDescriptions();

        return Observable.create(new ObservableOnSubscribe<Description>() {
            @Override
            public void subscribe(ObservableEmitter<Description> emitter) throws Exception {
                for (Description des : descriptionList) {
                    if (!emitter.isDisposed()) {
                        emitter.onNext(des);
                    }
                }
                if (!emitter.isDisposed()) {
                    emitter.onComplete();
                }
            }
        });
    }

    private List<Description> prepareDescriptions() {
        List<Description> descriptions = new ArrayList<>();
        descriptions.add(new Description(1, "Getting Notification!"));
        descriptions.add(new Description(2, "Going University!"));
        descriptions.add(new Description(3, "Meeting with Sir!"));
        descriptions.add(new Description(4, "Get together with friends!"));

        return descriptions;
    }

    class Description {
        int id;
        String description;

        public Description(int id, String description) {
            this.id = id;
            this.description = description;
        }

        public int getId() {
            return id;
        }

        public String getDescription() {
            return description;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }
}
